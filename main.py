import design_ver_2
import sys
import os
import pandas as pd
from datetime import date, timedelta, datetime
from PyQt5 import QtWidgets
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QApplication, QMainWindow
import plotly.graph_objects as go
import plotly


def find_data_file(filename):
    if getattr(sys, 'frozen', False):
        datadir = os.path.dirname(sys.executable)
    else:
        datadir = os.path.dirname(__file__)
    return os.path.join(datadir, filename)

rate_today = pd.read_html("https://bank.gov.ua/ua/markets/exchangerates", encoding = 'utf-8', thousands=".", decimal=",")[0]
rate_today.columns = ['num_code', 'code', 'amount', 'name', 'official rate']
rate_today = rate_today.drop('num_code', axis='columns')
rate_today['official rate'] = rate_today['official rate']/rate_today['amount']
rate_today = rate_today.drop('amount', axis='columns')
rate_today = pd.DataFrame({'code':'UAH','name':'Гривня','official rate':1.0},index=['zero']).append(rate_today).reset_index(drop=True)

file = open(find_data_file("last_date.txt"))
s = file.readlines()[-1]
last_date = datetime.strptime(s.replace('\n', ''), '%Y-%m-%d').date()
big_df = pd.read_csv(find_data_file('DATA.csv'))
big_df = big_df.drop('Unnamed: 0', axis='columns')
delta = (date.today() - last_date)
for n in range(1,delta.days+1):
    lost_time = (last_date + timedelta(days=n))
    one_day = pd.read_html("https://bank.gov.ua/ua/markets/exchangerates?date={}&period=daily".format(lost_time.strftime("%d.%m.%Y")), encoding = 'utf-8', thousands=".", decimal=",")[0]
    one_day.columns = ['num_code', 'code', 'amount', 'name', 'official rate']
    part = dict(zip(one_day['code'],one_day['official rate']/one_day['amount']))
    part['UAH'] = 1.0
    part['date'] = lost_time.strftime("%Y-%m-%d")
    big_df = big_df.append(pd.Series(part), ignore_index=True)

big_df.to_csv(find_data_file('DATA.csv'))
with open(find_data_file('last_date.txt'), 'a') as file: 
    file.write(str(date.today()) + "\n") 
file.close()

def is_number_repl_isdigit(s):
    return s.replace('.','',1).isdigit()

def create_plot(a, b):
    fig = go.Figure()
    fig.add_trace(
        go.Scatter(x=list(a), y=list(b)))
    fig.update_layout(
        autosize=True,
        margin=dict(
            l=5,
            r=5,
            b=5,
            t=10
        ),
        template="plotly_white"
    )
    fig.update_layout(
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label="1міс.",
                        step="month",
                        stepmode="backward"),
                    dict(count=6,
                        label="6міс.",
                        step="month",
                        stepmode="backward"),
                    dict(count=1,
                        label="поточний рік",
                        step="year",
                        stepmode="todate"),
                    dict(count=1,
                        label="1рік",
                        step="year",
                        stepmode="backward"),
                    dict(label="весь період",
                         step="all")
                ]),
                font=dict(size=18),
                borderwidth=0.25,
                bgcolor="#ffffff",
                activecolor="#e6e6e6",
                bordercolor="#444444",

            ),
            type="date"
        )
    )
    html = '<html><body>'
    html += plotly.offline.plot(fig, output_type='div', include_plotlyjs='cdn')
    html += '</body></html>'
        
    return html


class ExampleApp(QtWidgets.QMainWindow, design_ver_2.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setFixedSize(1080, 720)
        self.amount_1.textEdited.connect(self.onEdited_amount_1)
        self.currency_1.addItems(list(rate_today['name']))
        self.amount_2.textEdited.connect(self.onEdited_amount_2)
        self.currency_2.addItems(list(rate_today['name']))
        self.currency_1.setCurrentIndex(rate_today.index[rate_today['code'] == 'USD'][0])
        
        self.currency_1.activated.connect(self.onActivated_1)
        self.currency_2.activated.connect(self.onActivated_2)
        
        self.currency_1.currentIndexChanged.connect(self.setLabel_1)
        self.label_cur_1.setText(self.currency_1.currentText())
        self.currency_2.currentIndexChanged.connect(self.setLabel_2)
        self.label_cur_2.setText(self.currency_2.currentText())
        self.label_amount.setText(str(((rate_today.loc[rate_today['code'] == 'USD']['official rate']).values[0]).round(3)))
        self.graph.setHtml(create_plot(big_df['date'], big_df['USD']))

    
    def onEdited_amount_1(self, text):
        if is_number_repl_isdigit(self.amount_1.text()):
            df_1 = rate_today.loc[rate_today['name'] == self.currency_1.currentText()]
            df_2 = rate_today.loc[rate_today['name'] == self.currency_2.currentText()]
            text = str((float(text)*(df_1['official rate'].values/df_2['official rate'].values)[0]).round(4))
            self.amount_2.setText(text)
        else:
            self.amount_1.backspace()
        
    def onEdited_amount_2(self, text):
        if is_number_repl_isdigit(self.amount_2.text()):
            df_1 = rate_today.loc[rate_today['name'] == self.currency_1.currentText()]
            df_2 = rate_today.loc[rate_today['name'] == self.currency_2.currentText()]
            text = str((float(text)*(df_2['official rate'].values/df_1['official rate'].values)[0]).round(4))
            self.amount_1.setText(text)
        else:
            self.amount_2.backspace()
        
    def onActivated_1(self):
        if not self.amount_1.text():
            self.amount_1.setText('1')
        df_1 = rate_today.loc[rate_today['name'] == self.currency_1.currentText()]
        df_2 = rate_today.loc[rate_today['name'] == self.currency_2.currentText()]
        text = str((float(self.amount_1.text())*(df_1['official rate'].values/df_2['official rate'].values)[0]).round(4))
        self.amount_2.setText(text)
        self.graph.setHtml(create_plot(big_df['date'], 
                                       big_df[rate_today.loc[rate_today['name'] == self.currency_1.currentText()]['code'].values[0]]/
                                       big_df[rate_today.loc[rate_today['name'] == self.currency_2.currentText()]['code'].values[0]]))

        
    def onActivated_2(self):
        if not self.amount_1.text():
            self.amount_1.setText('1')
        df_1 = rate_today.loc[rate_today['name'] == self.currency_1.currentText()]
        df_2 = rate_today.loc[rate_today['name'] == self.currency_2.currentText()]
        text = str((float(self.amount_1.text())*(df_1['official rate'].values/df_2['official rate'].values)[0]).round(4))
        self.amount_2.setText(text)
        self.graph.setHtml(create_plot(big_df['date'], 
                                       big_df[rate_today.loc[rate_today['name'] == self.currency_1.currentText()]['code'].values[0]]/
                                       big_df[rate_today.loc[rate_today['name'] == self.currency_2.currentText()]['code'].values[0]]))

    
    def setLabel_1(self):
        self.label_cur_1.setText(self.currency_1.currentText())
        df_1 = rate_today.loc[rate_today['name'] == self.currency_1.currentText()]
        df_2 = rate_today.loc[rate_today['name'] == self.currency_2.currentText()]
        self.label_amount.setText(str((df_1['official rate'].values/df_2['official rate'].values)[0].round(3)))
        
    def setLabel_2(self):
        self.label_cur_2.setText(self.currency_2.currentText())
        df_1 = rate_today.loc[rate_today['name'] == self.currency_1.currentText()]
        df_2 = rate_today.loc[rate_today['name'] == self.currency_2.currentText()]
        self.label_amount.setText(str((df_1['official rate'].values/df_2['official rate'].values)[0].round(3)))
          
        
def main():
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle('Fusion')
    window = ExampleApp() 
    window.show() 
    app.exec_()

if __name__ == '__main__':
    main()
