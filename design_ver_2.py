from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtWebEngineWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(1080, 720)
        MainWindow.setStyleSheet("background-color: white;")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame_3 = QtWidgets.QFrame(self.centralwidget)
        self.frame_3.setGeometry(QtCore.QRect(30, 169, 1021, 541))
        self.frame_3.setStyleSheet(".QFrame{border: 1px solid lightgray;\n"
                                   "border-radius: 10px;\n"
                                   "padding: 2px;}\n"
                                   "")
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.graph = QtWebEngineWidgets.QWebEngineView(self.frame_3)
        self.graph.setGeometry(QtCore.QRect(10, 10, 1001, 521))
        self.graph.setFocusPolicy(QtCore.Qt.NoFocus)
        self.graph.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.graph.setAutoFillBackground(False)
        self.graph.setObjectName("graph")
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(30, 20, 391, 131))
        self.frame_2.setStyleSheet(".QFrame{border: 1px solid lightgray;\n"
                                   "border-radius: 10px;\n"
                                   "padding: 2px;}")
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.label_one = QtWidgets.QLabel(self.frame_2)
        self.label_one.setGeometry(QtCore.QRect(20, 20, 31, 31))
        self.label_one.setStyleSheet("font: 14pt \"Microsoft JhengHei UI\";\n"
                                     "color: rgb(102, 102, 102);")
        self.label_one.setObjectName("label_one")
        self.widget = QtWidgets.QWidget(self.frame_2)
        self.widget.setGeometry(QtCore.QRect(40, 20, 341, 31))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_cur_1 = QtWidgets.QLabel(self.widget)
        self.label_cur_1.setStyleSheet("font: 14pt \"Microsoft JhengHei UI\";\n"
                                       "color: rgb(102, 102, 102);")
        self.label_cur_1.setObjectName("label_cur_1")
        self.horizontalLayout.addWidget(self.label_cur_1)
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setStyleSheet("font: 14pt \"Microsoft JhengHei UI\";\n"
                                   "color: rgb(102, 102, 102);")
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.widget1 = QtWidgets.QWidget(self.frame_2)
        self.widget1.setGeometry(QtCore.QRect(20, 60, 311, 49))
        self.widget1.setObjectName("widget1")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget1)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_amount = QtWidgets.QLabel(self.widget1)
        self.label_amount.setStyleSheet("font: 20pt \"Microsoft JhengHei UI\";\n"
                                        "color: rgb(0, 0, 0);")
        self.label_amount.setObjectName("label_amount")
        self.horizontalLayout_2.addWidget(self.label_amount)
        self.label_cur_2 = QtWidgets.QLabel(self.widget1)
        self.label_cur_2.setStyleSheet("font: 20pt \"Microsoft JhengHei UI\";\n"
                                       "color: rgb(0, 0, 0);")
        self.label_cur_2.setObjectName("label_cur_2")
        self.horizontalLayout_2.addWidget(self.label_cur_2)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(450, 20, 601, 131))
        self.frame.setStyleSheet(".QFrame{border: 1px solid lightgray;\n"
                                 "border-radius: 10px;\n"
                                 "padding: 2px;}")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.amount_1 = QtWidgets.QLineEdit(self.frame)
        self.amount_1.setGeometry(QtCore.QRect(20, 20, 211, 41))
        self.amount_1.setBaseSize(QtCore.QSize(0, 0))
        self.amount_1.setStyleSheet("background-color:rgb(255, 255, 255);\n"
                                    "border: 1px solid gray;\n"
                                    "border-radius: 7px;\n"
                                    "padding: 0 3px;\n"
                                    "font: 14pt \"Microsoft JhengHei UI\";\n"
                                    "focus { \n"
                                    "border: 2px solid black;\n"
                                    "border-radius: 3px;}")
        self.amount_1.setPlaceholderText("")
        self.amount_1.setObjectName("amount_1")
        self.currency_1 = QtWidgets.QComboBox(self.frame)
        self.currency_1.setGeometry(QtCore.QRect(270, 20, 301, 41))
        self.currency_1.setStyleSheet("QComboBox{\n"
                                      "    font: 12pt \"Microsoft JhengHei UI\";\n"
                                      "    selection-background-color: #999999;\n"
                                      "}\n"
                                      "QComboBox QAbstractItemView{\n"
                                      "    background-color: #ffffff;\n"
                                      "    color: #000000;\n"
                                      "    selection-color: rgb(0, 0, 0);\n"
                                      "\n"
                                      "}\n"
                                      "")
        self.currency_1.setCurrentText("")
        self.currency_1.setMaxVisibleItems(15)
        self.currency_1.setObjectName("currency_1")
        self.currency_2 = QtWidgets.QComboBox(self.frame)
        self.currency_2.setGeometry(QtCore.QRect(270, 80, 301, 41))
        self.currency_2.setStyleSheet("QComboBox{\n"
                                      "    font: 12pt \"Microsoft JhengHei UI\";\n"
                                      "    selection-background-color: #999999;\n"
                                      "}\n"
                                      "QComboBox QAbstractItemView{\n"
                                      "    background-color: #ffffff;\n"
                                      "    color: #000000;\n"
                                      "    selection-color: rgb(0, 0, 0);\n"
                                      "\n"
                                      "}")
        self.currency_2.setMaxVisibleItems(15)
        self.currency_2.setObjectName("currency_2")
        self.amount_2 = QtWidgets.QLineEdit(self.frame)
        self.amount_2.setGeometry(QtCore.QRect(20, 80, 211, 41))
        self.amount_2.setStyleSheet("background-color:rgb(255, 255, 255);\n"
                                    "border: 1px solid gray;\n"
                                    "border-radius: 7px;\n"
                                    "padding: 0 3px;\n"
                                    "font: 14pt \"Microsoft JhengHei UI\";\n"
                                    "focus { \n"
                                    "border: 2px solid black;\n"
                                    "border-radius: 3px;}")
        self.amount_2.setPlaceholderText("")
        self.amount_2.setObjectName("amount_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_one.setText(_translate("MainWindow", "1"))
        self.label_cur_1.setText(_translate("MainWindow", "TextLabel"))
        self.label_3.setText(_translate("MainWindow", "дорівнює"))
        self.label_amount.setText(_translate("MainWindow", "TextLabel"))
        self.label_cur_2.setText(_translate("MainWindow", "TextLabel"))

